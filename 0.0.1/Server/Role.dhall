let Prelude = ../Prelude.dhall

let SQL = ../SQL.dhall

let joinS = Prelude.concatSep " "

let Role
    : Type
    = { name : Text, clauses : List Text, sql : SQL.t }

let make
    : Text → List Text → Role
    = λ(name : Text) →
      λ(clauses : List Text) →
        let sql =
              [ SQL.create "CREATE ROLE ${name}  ${joinS clauses};"
              , SQL.drop "DROP ROLE ${name};"
              ]

        in  { name, clauses, sql }

in  { t = Role, make }
