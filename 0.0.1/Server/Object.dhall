let Database = ../Database/package.dhall

let Role = ./Role.dhall

let Grant = ./Grant.dhall

let Object = < Database : Database.t | Role : Role.t | Grant : Grant.t >

let database
    : Text → List Database.ot → Object
    = λ(name : Text) →
      λ(objects : List Database.ot) →
        Object.Database (Database.make name objects)

let role
    : Text → List Text → Object
    = λ(name : Text) →
      λ(clauses : List Text) →
        Object.Role (Role.make name clauses)

let grant
    : List Text → List Text → List Text → Object
    = λ(privileges : List Text) →
      λ(objects : List Text) →
      λ(roles : List Text) →
        Object.Grant (Grant.make privileges objects roles False)

let grantA
    : List Text → List Text → List Text → Object
    = λ(privileges : List Text) →
      λ(objects : List Text) →
      λ(roles : List Text) →
        Object.Grant (Grant.make privileges objects roles True)

in  { t = Object, database, role, grant, grantA }
