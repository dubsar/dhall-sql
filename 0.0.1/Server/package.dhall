let Server = ./Server.dhall

let Object = ./Object.dhall

in  { t = Server.t
    , ot = Object.t
    , make = Server.make
    , database = Object.database
    , role = Object.role
    , grant = Object.grant
    , grantA = Object.grantA
    }
