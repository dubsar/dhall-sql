let Object = ./Object.dhall

let Server
    : Type
    = { objects : List Object.t }

let make
    : List Object.t → Server
    = λ(objects : List Object.t) → { objects }

in  { t = Server, make }
