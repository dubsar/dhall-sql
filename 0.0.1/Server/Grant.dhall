let Prelude = ../Prelude.dhall

let SQL = ../SQL.dhall

let joinS = Prelude.concatSep " "

let Grant
    : Type
    = { privileges : List Text
      , objects : List Text
      , roles : List Text
      , admin : Bool
      , sql : SQL.t
      }

let make
    : List Text → List Text → List Text → Bool → Grant
    = λ(privileges : List Text) →
      λ(objects : List Text) →
      λ(roles : List Text) →
      λ(admin : Bool) →
        let jointPrivileges = joinS privileges

        let jointObjects = joinS objects

        let jointRoles = joinS roles

        let withAdmin = if admin then " WITH ADMIN OPTION" else ""

        let sql =
              [ SQL.create
                  "GRANT ${jointPrivileges} ON  ${jointObjects} TO ${jointRoles} ${withAdmin};"
              , SQL.drop
                  "REVOKE ${jointPrivileges} ON  ${jointObjects} FROM ${jointRoles};"
              ]

        in  { privileges, objects, roles, admin, sql }

in  { t = Grant, make }
