let Table = ./Table/package.dhall

let Database = ./Database/package.dhall

let Server = ./Server/package.dhall

let Builder =
      { Type =
          { server : List Server.ot → Server.t
          , database : Text → List Database.ot → Server.ot
          , role : Text → List Text → Server.ot
          , grant : List Text → List Text → List Text → Server.ot
          , grantA : List Text → List Text → List Text → Server.ot
          , schema : Text → Database.ot
          , table : Text → List Table.ot → Database.ot
          , col : Text → Table.ot
          , pk : List Text → Table.ot
          , fk : List Text → Text → List Text → Table.ot
          , uniq : List Text → Table.ot
          }
      , default =
        { server = Server.make
        , database = Server.database
        , role = Server.role
        , grant = Server.grant
        , grantA = Server.grantA
        , schema = Database.schema
        , table = Database.table
        , col = Table.col
        , pk = Table.pk
        , fk = Table.fk
        , uniq = Table.uniq
        }
      }

in  { Builder }
