let Prelude = ./Prelude.dhall

let idT = Prelude.identity Text

let Command = < Create : Text | Drop : Text | Wild : Text >

let bool = λ(bool : Bool) → λ(_ : Text) → bool

let isCreate =
      λ(command : Command) →
        merge
          { Create = bool True, Drop = bool False, Wild = bool False }
          command

let isDrop =
      λ(command : Command) →
        merge
          { Create = bool False, Drop = bool True, Wild = bool False }
          command

let isWild =
      λ(command : Command) →
        merge
          { Create = bool False, Drop = bool False, Wild = bool True }
          command

let SQL = List Command

let empty
    : SQL
    = [] : List Command

let filterC = Prelude.filter Command

let filterCreate = λ(sql : SQL) → filterC isCreate sql

let filterDrop = λ(sql : SQL) → filterC isDrop sql

let filterWild = λ(sql : SQL) → filterC isWild sql

let renderCommand
    : Command → Text
    = λ(command : Command) →
        merge { Create = idT, Drop = idT, Wild = idT } command

let render =
      λ(separator : Text) →
        Prelude.compose
          SQL
          (List Text)
          Text
          (Prelude.map Command Text renderCommand)
          (Prelude.concatSep separator)

let renderFirst =
      λ(sql : SQL) →
        merge { Some = renderCommand, None = "" } (List/head Command sql)

in  { t = SQL
    , create = Command.Create
    , drop = Command.Drop
    , empty
    , filterCreate
    , filterDrop
    , filterWild
    , render
    , renderCommand
    , renderFirst
    , wild = Command.Wild
    }
