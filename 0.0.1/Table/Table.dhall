let Object = ./Object.dhall

let SQL = ../SQL.dhall

let render = ./render.dhall

let Table
    : Type
    = { name : Text, objects : List Object.t, sql : SQL.t }

let make
    : Text → List Object.t → Table
    = λ(name : Text) →
      λ(objects : List Object.t) →
        let sql =
              [ SQL.create (render.create name objects)
              , SQL.drop (render.drop name)
              ]

        in  { name, objects, sql }

in  { t = Table
    , ot = Object.t
    , make
    , col = Object.col
    , uniq = Object.uniq
    , pk = Object.pk
    , fk = Object.fk
    }
