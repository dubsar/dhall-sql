let ForeignKey
    : Type
    = { columns : List Text, reftable : Text, refcolumns : List Text }

let make
    : List Text → Text → List Text → ForeignKey
    = λ(columns : List Text) →
      λ(reftable : Text) →
      λ(refcolumns : List Text) →
        { columns, reftable, refcolumns }

in  { t = ForeignKey, make }
