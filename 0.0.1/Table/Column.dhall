let Column
    : Type
    = { sql : Text }

let make
    : Text → Column
    = λ(sql : Text) → { sql }

in  { t = Column, make }
