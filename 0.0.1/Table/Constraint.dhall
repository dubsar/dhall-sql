let ForeignKey = ./ForeignKey.dhall

let Constraint =
      < Unique : List Text
      | PrimaryKey : List Text
      | ForeignKey : ForeignKey.t
      >

let unique
    : List Text → Constraint
    = λ(list : List Text) → Constraint.Unique list

let pk
    : List Text → Constraint
    = λ(list : List Text) → Constraint.PrimaryKey list

let fk
    : List Text → Text → List Text → Constraint
    = λ(columns : List Text) →
      λ(reftable : Text) →
      λ(refcolumns : List Text) →
        Constraint.ForeignKey (ForeignKey.make columns reftable refcolumns)

in  { t = Constraint, unique, pk, fk }
