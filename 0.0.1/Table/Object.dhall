let Column = ./Column.dhall

let Constraint = ./Constraint.dhall

let Object = < Column : Column.t | Constraint : Constraint.t >

let col
    : Text → Object
    = λ(sql : Text) → Object.Column (Column.make sql)

let uniq
    : List Text → Object
    = λ(list : List Text) → Object.Constraint (Constraint.unique list)

let pk
    : List Text → Object
    = λ(list : List Text) → Object.Constraint (Constraint.pk list)

let fk
    : List Text → Text → List Text → Object
    = λ(columns : List Text) →
      λ(reftable : Text) →
      λ(refcolumns : List Text) →
        Object.Constraint (Constraint.fk columns reftable refcolumns)

in  { t = Object, col, uniq, pk, fk }
