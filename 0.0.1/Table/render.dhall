let Object = ./Object.dhall

let Column = ./Column.dhall

let Constraint = ./Constraint.dhall

let ForeignKey = ./ForeignKey.dhall

let Prelude = ../Prelude.dhall

let join = Prelude.concatSep

let emptyT = Prelude.empty Text

let isEmptyT = Prelude.null Text

let column
    : Column.t → List Text
    = λ(column : Column.t) → [ "${column.sql}" ]

let pk
    : List Text → List Text
    = λ(list : List Text) →
        if    isEmptyT list
        then  list
        else  [ "PRIMARY KEY (" ++ join ", " list ++ ")" ]

let unique
    : List Text → List Text
    = λ(list : List Text) →
        if isEmptyT list then list else [ "UNIQUE (" ++ join ", " list ++ ")" ]

let fk
    : ForeignKey.t → List Text
    = λ(fk : ForeignKey.t) →
        let columns = join ", " fk.columns

        let refcolumns = join ", " fk.refcolumns

        in  [     "FOREIGN KEY ("
              ++  columns
              ++  ") REFERENCES "
              ++  fk.reftable
              ++  " ("
              ++  refcolumns
              ++  ")"
            ]

let constraint
    : Constraint.t → List Text
    = λ(constraint : Constraint.t) →
        merge { Unique = unique, PrimaryKey = pk, ForeignKey = fk } constraint

let object
    : Object.t → List Text
    = λ(object : Object.t) →
        merge { Column = column, Constraint = constraint } object

let objects
    : List Object.t → List Text
    = λ(objects : List Object.t) →
        List/fold
          Object.t
          objects
          (List Text)
          (λ(obj : Object.t) → λ(list : List Text) → object obj # list)
          emptyT

let create
    : Text → List Object.t → Text
    = λ(name : Text) →
      λ(list : List Object.t) →
        let objects =
              join
                ''
                ,
                	''
                (objects list)

        in  ''
            CREATE TABLE IF NOT EXISTS ${name} (
            ${"\t"}${objects}
            );
            ''

let drop
    : Text → Text
    = λ(name : Text) → "DROP TABLE IF EXISTS ${name};"

in  { create, drop }
