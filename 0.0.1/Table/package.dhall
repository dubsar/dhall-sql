let Table = ./Table.dhall

in  { t = Table.t
    , ot = Table.ot
    , make = Table.make
    , pk = Table.pk
    , fk = Table.fk
    , uniq = Table.uniq
    , col = Table.col
    }
