let concatSep =
      ./Prelude/Text/concatSep ? https://prelude.dhall-lang.org/Text/concatSep

let empty = ./Prelude/List/empty ? https://prelude.dhall-lang.org/List/empty

let identity =
        ./Prelude/Function/identity
      ? https://prelude.dhall-lang.org/Function/identity

let map = ./Prelude/List/map ? https://prelude.dhall-lang.org/List/map

let null = ./Prelude/List/null ? https://prelude.dhall-lang.org/List/null

let filter = ./Prelude/List/filter ? https://prelude.dhall-lang.org/List/filter

let compose =
        ./Prelude/Function/compose
      ? https://prelude.dhall-lang.org/Function/compose

in  { concatSep, empty, identity, map, null, filter, compose }
