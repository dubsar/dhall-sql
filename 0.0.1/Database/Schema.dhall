let SQL = ../SQL.dhall

let Schema
    : Type
    = { name : Text, sql : SQL.t }

let make
    : Text → Schema
    = λ(name : Text) →
        let sql =
              [ SQL.create "CREATE SCHEMA IF NOT EXISTS ${name};"
              , SQL.drop "DROP SCHEMA IF EXISTS ${name};"
              ]

        in  { name, sql }

in  { t = Schema, make }
