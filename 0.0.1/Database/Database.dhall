let Object = ./Object.dhall

let SQL = ../SQL.dhall

let render = ./render.dhall

let Database
    : Type
    = { name : Text, objects : List Object.t, sql : SQL.t }

let make
    : Text → List Object.t → Database
    = λ(name : Text) →
      λ(objects : List Object.t) →
        let sql =
              [ SQL.create (render.create name), SQL.drop (render.drop name) ]

        in  { name, objects, sql }

in  { t = Database, make }
