let create
    : Text → Text
    = λ(name : Text) → "CREATE DATABASE ${name};"

let drop
    : Text → Text
    = λ(name : Text) → "DROP DATABASE IF EXISTS ${name};"

in  { create, drop }
