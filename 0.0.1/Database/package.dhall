let Database = ./Database.dhall

let Object = ./Object.dhall

in  { t = Database.t
    , ot = Object.t
    , make = Database.make
    , render = ./render.dhall
    , schema = Object.schema
    , table = Object.table
    }
