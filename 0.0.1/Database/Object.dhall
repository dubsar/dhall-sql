let Schema = ./Schema.dhall

let Table = ../Table/package.dhall

let Object = < Schema : Schema.t | Table : Table.t >

let table
    : Text → List Table.ot → Object
    = λ(name : Text) →
      λ(objects : List Table.ot) →
        Object.Table (Table.make name objects)

let schema
    : Text → Object
    = λ(name : Text) → Object.Schema (Schema.make name)

in  { t = Object, schema, table }
