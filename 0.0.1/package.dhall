let DSL = ./DSL.dhall

in  { Database = ./Database/package.dhall
    , Server = ./Server/package.dhall
    , Table = ./Table/package.dhall
    , Builder = DSL.Builder
    }
