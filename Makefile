VERSION=0.0.1
URL=https://dhall-sql.deno.dev
DOCS=../dhall-sql-deno/public/docs

.PHONY: docs
docs:
	dhall-docs \
		--input ./$(VERSION) \
		--package-name dhall-sql:$(VERSION) \
		--output-link dhall-docs \
		--base-import-url $(URL)/$(VERSION)
	rm -rf $(DOCS)
	cp -Lr dhall-docs $(DOCS)
